#!/bin/bash

mkdir -p src/{controllers,models,views,public}
mkdir -p src/public/{css,js,images}

touch src/index.php
touch src/public/css/style.css
touch src/public/js/script.js

echo "<!DOCTYPE html>
<html>
<head>
    <title>Mon Site de Prise de Notes</title>
    <link rel='stylesheet' href='css/style.css'>
</head>
<body>
    <script src='js/script.js'></script>
</body>
</html>" > src/views/template.php

echo "<?php
// Votre code PHP pour le routeur et la logique principale." > src/index.php
